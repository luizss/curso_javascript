// funcao sem retorno
function imprimirSoma(a, b) 
{
    console.log(a + b)
}

imprimirSoma(3,5)
imprimirSoma(2)
imprimirSoma(2, 10, 4, 5, 6, 7, 8)
imprimirSoma()

//funcao com retorno

function soma(a, b = 0)
{
    return a + b
}
console.log(soma(6,8))
console.log(soma(3))
console.log(soma())