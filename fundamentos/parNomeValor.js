// par nome/valor
const saudacao = 'Olá' //constextp léxico 1

function exec(){
    const saudacao = 'Fala' //constexto léxico 2
    return saudacao
}

//objetos são grupos aninhados de pares nome/valor
const cliente = {
    nome: 'Pedro',
    idade: 32,
    peso: 90,
    endereco: {
        logradouro: 'Rua Tobias Barreto',
        numero: 396
    }
}

console.log(saudacao)
console.log(exec())
console.log(cliente)